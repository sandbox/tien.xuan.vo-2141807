Module: Facebook Username
Author: Tien Vo Xuan <https://drupal.org/user/2535842>


Description
===========

This module provides a simple field for handling Facebook username,
which can be added to content types for validating and storing a
Facebook username.
This module only checks the syntax, not that the username really exists.

It defines 2 formatters, "plain text" and "link", where the username is
prefixed by @. Both formatters are themeable.


Installation
============

Copy the 'facebook_username' module directory in to your Drupal
sites/all/modules directory as usual.

Enable the facebook_username module by navigating to
administer > modules

Credits
=======

This module is based on Twitter username
<https://drupal.org/project/twitter_username>, written by Opi
<http://drupal.org/user/263678>.
